#!/bin/bash

die()
{
    echo "$1" >&2
    exit 1;
}

THISSCRIPTDIR=$(dirname $(readlink -f $0))
pushd ${THISSCRIPTDIR} >/dev/null

SRCDIR="${THISSCRIPTDIR}/src"
PATCHDIR="${SRCDIR}/patches"

PACKAGES="dnsrewrite dnsrewrite-ui goahead" 

CONFIGDIR="${THISSCRIPTDIR}/src/config"
#DEFCONFIG=TM02
DEFCONFIG=NEXX

OPENWRT="${THISSCRIPTDIR}/openwrt"
LOCALPKG="${THISSCRIPTDIR}/modules"

PREPFILE=${THISSCRIPTDIR}/.prep_done

if [[ -f $PREPFILE ]] ; then
    PREPPED=1
else
    PREPPED=0
fi

touch $PREPFILE

#git submodule init
if [[ $PREPPED == 0 ]] ; then
    git submodule update --init --recursive

    #Checkout openwrt
    #git submodule add git://git.openwrt.org/15.05/openwrt.git || die "failed to clone OpenWRT"

    pushd ${OPENWRT} >/dev/null

    #This resets the repo to a version we know 'works'
    #good for pinning!

    #Do a checkout and then commit to update the pinned submodule
    #git checkout eadf19c0b43d2f75f196ea8d875a08c7c348530c

    #Usefult command, resets rather than checks out
    #git reset --hard eadf19c0b43d2f75f196ea8d875a08c7c348530c


    #Update all the openwrt feeds
    ./scripts/feeds update -a || die "failed to update feeds"

    #Install them all
    ./scripts/feeds install -a || die "failed to install all feeds"

    popd >/dev/null

    #download our packages
    for package in ${PACKAGES} ; do
        WRTPKGDIR="${OPENWRT}/package"
        ln -sv "${LOCALPKG}/$package" "${WRTPKGDIR}/$package"
        pushd ${LOCALPKG} >/dev/null
        git -C $package checkout master
        if [[ -x $package/prep.sh ]] ; then
            $package/prep.sh
        fi
        popd >/dev/null
    done


    pushd ${LOCALPKG} >/dev/null
    git -C goahead-upstream checkout master
    popd >/dev/null
else
    #update our packages
    for package in ${PACKAGES} ; do
        pushd ${LOCALPKG} >/dev/null
        git -C $package pull
        popd >/dev/null
    done
fi
#
#mkdir "${LOCALPKG}"



#link our files in place

#Patches - 2 types
#Type 1 - placed into the appropriate openwrt patch directory (via symlink) to be applied during the openwrt build
for patch in ${PATCHDIR}/type1.* ; do
    echo "Linking OpenWRT applied patch $(basename $patch)"
    PATCHPATH="$(echo $(basename ${patch}) | sed -e 's#type1.##' -e 's#\.#/#g')"
    LINKPATH="$(echo $(dirname ${PATCHPATH}) | sed -e 's#[^/]*#..#g')"
    SYMLINK="${LINKPATH}/src/patches/$(basename ${patch})"

    mkdir -p $(dirname ${PATCHPATH})
    ln -sfv ${SYMLINK} "${THISSCRIPTDIR}/${PATCHPATH}"
done

#Type 2 - Applied directly to the openwrt codebase (for Makefile changes etc)
pushd openwrt >/dev/null

for patch in ${PATCHDIR}/type2.* ; do
    echo "Applying patch $(basename $patch)"
    git apply $patch
done

popd >/dev/null

#config
mv ${OPENWRT}/.config ${OPENWRT}/.config.blxorig
ln -s "../src/config/${DEFCONFIG}.config" ${OPENWRT}/.config

pushd ${OPENWRT} > /dev/null
make defconfig

if diff ${OPENWRT}/.config ${CONFIGDIR}/${DEFCONFIG}.config >/dev/null ; then
    mv ${CONFIGDIR}/${DEFCONFIG}.config ${CONFIGDIR}/${DEFCONFIG}.config.orig
    mv ${OPENWRT}/.config ${CONFIGDIR}/${DEFCONFIG}.config
    ln -s "../src/config/${DEFCONFIG}.config" ${OPENWRT}/.config
    echo "*******************************"
    echo "OPENWRT DEFAULT CONFIGURATION CHANGED"
    echo "FILES IN SOURCE TREE OVERWRITTEN - USE git to resolve, or pin to a known version"
    echo "*******************************"
fi
popd >/dev/null

#dl files (we archive these becuase sometimes they are not available anymore)
#they are likely to change if we change target hardware, so this needs to be updated if the hardware changes.

mkdir -p ${OPENWRT}/dl/
cp -v ${THISSCRIPTDIR}/src/dl/* ${OPENWRT}/dl/

#useful info
echo "*******************************"
echo "Protexm packages"
echo ""
for package in ${PACKAGES} ; do
    PKGDIR="${OPENWRT}/package"
    echo "${PKGDIR}/${package}"
done
echo ""
echo "*******************************"

